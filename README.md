# API Communication Scheduler

## Tecnologias utilizadas
 - [X] Springboot
 - [X] Maven
 - [X] Spring Data
 - [X] Swagger
 - [X] Docker
 - [X] Mockito
 - [X] JUnit
 - [X] Liquibase

***
### Versionamento
 - Utilizado branchs com o prefixo *feature* para novas funcionalidades
    * A cada finalização de uma nova feature é criado um merge request para aprovação e merge em develop
 - Utilizado a branch de develop para ambiente de desenvolvimento
 - Utilizado a branch de homolog para ambiente de homologação
 - Utilizado a branch main para o ambiente estável em produção
***
### Testes Funcionais com Cucumber
 Para sugestão de testes fututos, sugiro também a utilização do Cucumber, pois o relatório extraído, ajuda 
bastante o acompanhamento de testes com linguagem mais próxima ao cliente final.

***

## Documentação

### Deploy com docker

Para realizar o deploy da aplicação e execução do projeto, via container utilizando docker, será necessário a execução
dos passos abaixo no terminal:

```
./mvnw clean package -DskipTests

docker-compose up -d

./mvnw liquibase:update 
```
Após concluir validar se o container está disponível no endereço: http://localhost:8080/swagger-ui.html

***

## Kafka

Como sugestão, para projetos futuros para implementação das funcionalidades de envio de mensagens, acredito que o Kafka
poderá atender bem aos requisitos, trabalhando com tópicos e envio de mensagens através de um listener.

Caso seja uma consideração válida, utilizei o seguintes comandos abaixo para subir localmente o serviço do kafka:

```
cd kafka

docker-compose up -d
```

### Código

#### Para executar o ambiente do kafka local:

* A Classe **kafka/listener/CommunicationListener.java** contém um exemplo de listener do Kafka
* Adicionar nos hosts o nome kafka apontando para localhost
* Alterar no **application.yml** *kafka.active* para true

**Obs:** *Deverá alterar o nome do listener na classe MagaluListener.java para o nome que foi criado
no postman*

### Postman
Para criação de tópicos deixei uma coleção do postman na pasta **kafka/collections** , lá encontra-se os endpoints
para criação de novos tópicos com consultas no banco.

**Obs**: *Alterar body onde contem ${}*


***