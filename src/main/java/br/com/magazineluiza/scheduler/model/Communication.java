package br.com.magazineluiza.scheduler.model;

import br.com.magazineluiza.scheduler.enums.FormatCommunicationEnum;
import br.com.magazineluiza.scheduler.enums.StatusCommunicationEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDateTime;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "TB_COMMUNICATION")
public class Communication implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;

    @NotNull
    @Column(name = "DT_EXECUTION", columnDefinition = "TIMESTAMP", nullable = false)
    private LocalDateTime executionDate;

    @NotBlank
    @Column(name = "RECIPIENT")
    private String recipient;

    @NotBlank
    @Column(name = "MESSAGE")
    private String message;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "STATUS")
    private StatusCommunicationEnum statusCommunication;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "FORMAT")
    private FormatCommunicationEnum formatCommunication;
}
