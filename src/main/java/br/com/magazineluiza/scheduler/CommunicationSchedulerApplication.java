package br.com.magazineluiza.scheduler;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CommunicationSchedulerApplication {

	public static void main(String[] args) {
		SpringApplication.run(CommunicationSchedulerApplication.class, args);
	}

}
