package br.com.magazineluiza.scheduler.service;

import br.com.magazineluiza.scheduler.dto.request.CreateCommunicationRequest;
import br.com.magazineluiza.scheduler.dto.response.CommunicationResponse;
import br.com.magazineluiza.scheduler.enums.FormatCommunicationEnum;
import br.com.magazineluiza.scheduler.enums.StatusCommunicationEnum;
import br.com.magazineluiza.scheduler.exception.NotFoundCommunicationException;
import br.com.magazineluiza.scheduler.mappers.CommunicationMapper;
import br.com.magazineluiza.scheduler.model.Communication;
import br.com.magazineluiza.scheduler.repository.CommunicationRepository;
import br.com.magazineluiza.scheduler.validator.communicationformat.ValidateCommunicationFormat;
import br.com.magazineluiza.scheduler.validator.communicationstatus.ValidateCommunicationStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CommunicationService {

    @Autowired
    private CommunicationRepository communicationRepository;

    @Autowired
    private List<ValidateCommunicationFormat> validators;

    @Autowired
    private List<ValidateCommunicationStatus> validatorsStatus;

    public CommunicationResponse create(CreateCommunicationRequest communicationRequest) {
        Communication communication = CommunicationMapper.INSTANCE.createCommunicationRequestToCommunication(communicationRequest);
        validateFormats(communicationRequest.getFormatCommunication(), communicationRequest.getRecipient());
        communication.setStatusCommunication(StatusCommunicationEnum.SCHEDULED);
        communication = communicationRepository.save(communication);
        return CommunicationMapper.INSTANCE.communicationToCommunicationResponse(communication);
    }

    public StatusCommunicationEnum findStatusCommunicationById(Long id) {
        Communication communication = findById(id);
        return communication.getStatusCommunication();
    }

    public CommunicationResponse cancelCommunicationById(Long id) {
        Communication communication = findById(id);
        validateStatus(communication.getStatusCommunication());
        communication.setStatusCommunication(StatusCommunicationEnum.CANCELLED);
        Communication communicationSaved = communicationRepository.save(communication);
        return CommunicationMapper.INSTANCE.communicationToCommunicationResponse(communicationSaved);
    }

    private Communication findById(Long id) {
        return communicationRepository.findById(id).orElseThrow(() -> new NotFoundCommunicationException());
    }

    private void validateFormats(FormatCommunicationEnum formatCommunication, String valueFormat) {
        validators.forEach(validator -> {
            validator.validate(formatCommunication, valueFormat);
        });
    }

    private void validateStatus(StatusCommunicationEnum status) {
        validatorsStatus.forEach(validator -> {
            validator.validate(status);
        });
    }

}
