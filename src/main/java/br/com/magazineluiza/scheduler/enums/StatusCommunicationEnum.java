package br.com.magazineluiza.scheduler.enums;

public enum StatusCommunicationEnum {
    SCHEDULED, CANCELLED, FAILED, SUCCESS
}
