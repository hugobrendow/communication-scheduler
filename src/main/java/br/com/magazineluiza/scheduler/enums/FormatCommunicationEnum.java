package br.com.magazineluiza.scheduler.enums;

public enum FormatCommunicationEnum {
    EMAIL, WHATSAPP, PUSH, SMS
}
