package br.com.magazineluiza.scheduler.repository;

import br.com.magazineluiza.scheduler.model.Communication;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CommunicationRepository extends JpaRepository<Communication, Long> {

}
