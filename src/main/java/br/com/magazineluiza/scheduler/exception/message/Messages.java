package br.com.magazineluiza.scheduler.exception.message;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum Messages {
    TEMP("message.temp");

    @Getter
    private final String value;
}