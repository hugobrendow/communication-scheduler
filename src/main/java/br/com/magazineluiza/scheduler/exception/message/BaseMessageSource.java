package br.com.magazineluiza.scheduler.exception.message;

import br.com.magazineluiza.scheduler.exception.AppErrors;
import org.springframework.stereotype.Component;

@Component
public interface BaseMessageSource {
    String getMessage(String resource, Object[] params);
    String getMessage(AppErrors error, Object[] params);
}