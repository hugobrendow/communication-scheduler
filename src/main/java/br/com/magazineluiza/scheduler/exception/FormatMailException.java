package br.com.magazineluiza.scheduler.exception;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class FormatMailException extends IllegalArgumentException {
    public FormatMailException(String message) {
        super(message);
    }
}
