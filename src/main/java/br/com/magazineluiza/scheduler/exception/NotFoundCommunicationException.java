package br.com.magazineluiza.scheduler.exception;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class NotFoundCommunicationException extends IllegalArgumentException {
    public NotFoundCommunicationException(String message) {
        super(message);
    }
}
