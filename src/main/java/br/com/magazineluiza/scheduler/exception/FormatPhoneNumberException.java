package br.com.magazineluiza.scheduler.exception;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class FormatPhoneNumberException extends IllegalArgumentException {
    public FormatPhoneNumberException(String message) {
        super(message);
    }
}
