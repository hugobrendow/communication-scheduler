package br.com.magazineluiza.scheduler.exception;

public abstract class BaseException extends RuntimeException {
    public abstract AppErrors error();
}
