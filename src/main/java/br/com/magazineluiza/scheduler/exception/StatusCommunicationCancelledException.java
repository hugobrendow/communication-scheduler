package br.com.magazineluiza.scheduler.exception;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class StatusCommunicationCancelledException extends IllegalArgumentException {
    public StatusCommunicationCancelledException(String message) {
        super(message);
    }
}
