package br.com.magazineluiza.scheduler.exception;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class StatusCommunicationExecutedException extends IllegalArgumentException {
    public StatusCommunicationExecutedException(String message) {
        super(message);
    }
}