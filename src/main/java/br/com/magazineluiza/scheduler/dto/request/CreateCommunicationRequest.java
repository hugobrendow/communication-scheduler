package br.com.magazineluiza.scheduler.dto.request;

import br.com.magazineluiza.scheduler.enums.FormatCommunicationEnum;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CreateCommunicationRequest {

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime executionDate;

    @NotBlank
    private String recipient;

    @NotBlank
    private String message;

    @NotNull
    private FormatCommunicationEnum formatCommunication;

}
