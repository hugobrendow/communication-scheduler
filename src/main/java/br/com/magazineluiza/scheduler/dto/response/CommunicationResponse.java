package br.com.magazineluiza.scheduler.dto.response;

import br.com.magazineluiza.scheduler.enums.FormatCommunicationEnum;
import br.com.magazineluiza.scheduler.enums.StatusCommunicationEnum;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

@Data
public class CommunicationResponse implements Serializable {
    private Long id;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime executionDate;

    private String recipient;
    private String message;
    private StatusCommunicationEnum statusCommunication;
    private FormatCommunicationEnum formatCommunication;
}
