package br.com.magazineluiza.scheduler.mappers;

import br.com.magazineluiza.scheduler.dto.request.CreateCommunicationRequest;
import br.com.magazineluiza.scheduler.dto.response.CommunicationResponse;
import br.com.magazineluiza.scheduler.model.Communication;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface CommunicationMapper {
    CommunicationMapper INSTANCE = Mappers.getMapper(CommunicationMapper.class);

     Communication createCommunicationRequestToCommunication(CreateCommunicationRequest communicationRequest);
     CommunicationResponse communicationToCommunicationResponse(Communication communication);
}
