package br.com.magazineluiza.scheduler.validator.communicationstatus;

import br.com.magazineluiza.scheduler.enums.StatusCommunicationEnum;
import br.com.magazineluiza.scheduler.exception.StatusCommunicationExecutedException;
import org.springframework.stereotype.Component;

@Component
public class ValidateCommunicationExecuted implements ValidateCommunicationStatus {

    @Override
    public void validate(StatusCommunicationEnum statusCommunicationEnum) {
        if (StatusCommunicationEnum.FAILED.equals(statusCommunicationEnum) ||
                StatusCommunicationEnum.SUCCESS.equals(statusCommunicationEnum)) {
            throw new StatusCommunicationExecutedException();
        }
    }
}
