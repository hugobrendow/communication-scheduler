package br.com.magazineluiza.scheduler.validator.communicationformat;

import br.com.magazineluiza.scheduler.enums.FormatCommunicationEnum;
import br.com.magazineluiza.scheduler.exception.FormatPhoneNumberException;
import br.com.magazineluiza.scheduler.util.ValidationUtils;
import org.springframework.stereotype.Component;

@Component
public class ValidateCommunicationMobilePhoneFormat implements ValidateCommunicationFormat {
    @Override
    public void validate(FormatCommunicationEnum formatCommunication, String valueMobilePhone) {
        if ((FormatCommunicationEnum.SMS.equals(formatCommunication) || FormatCommunicationEnum.WHATSAPP.equals(formatCommunication)
                || FormatCommunicationEnum.PUSH.equals(formatCommunication)) && (!ValidationUtils.isValidMobileNumber(valueMobilePhone))) {
            throw new FormatPhoneNumberException();
        }
    }
}
