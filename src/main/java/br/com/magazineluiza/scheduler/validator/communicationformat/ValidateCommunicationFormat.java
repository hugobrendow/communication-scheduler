package br.com.magazineluiza.scheduler.validator.communicationformat;

import br.com.magazineluiza.scheduler.enums.FormatCommunicationEnum;

public interface ValidateCommunicationFormat {
    void validate(FormatCommunicationEnum formatCommunication, String valueCommunicationFormat);
}
