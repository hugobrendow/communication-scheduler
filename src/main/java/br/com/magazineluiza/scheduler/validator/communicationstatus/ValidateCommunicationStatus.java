package br.com.magazineluiza.scheduler.validator.communicationstatus;

import br.com.magazineluiza.scheduler.enums.StatusCommunicationEnum;

public interface ValidateCommunicationStatus {
    void validate(StatusCommunicationEnum statusCommunicationEnum);
}
