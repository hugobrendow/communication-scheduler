package br.com.magazineluiza.scheduler.validator.communicationstatus;

import br.com.magazineluiza.scheduler.enums.StatusCommunicationEnum;
import br.com.magazineluiza.scheduler.exception.StatusCommunicationCancelledException;
import org.springframework.stereotype.Component;

@Component
public class ValidateCommunicationCancelled implements ValidateCommunicationStatus {

    @Override
    public void validate(StatusCommunicationEnum statusCommunicationEnum) {
        if (StatusCommunicationEnum.CANCELLED.equals(statusCommunicationEnum)) {
            throw new StatusCommunicationCancelledException();
        }
    }
}
