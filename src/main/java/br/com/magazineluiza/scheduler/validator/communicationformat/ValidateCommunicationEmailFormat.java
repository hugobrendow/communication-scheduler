package br.com.magazineluiza.scheduler.validator.communicationformat;

import br.com.magazineluiza.scheduler.enums.FormatCommunicationEnum;
import br.com.magazineluiza.scheduler.exception.FormatMailException;
import br.com.magazineluiza.scheduler.util.ValidationUtils;
import org.springframework.stereotype.Component;

@Component
public class ValidateCommunicationEmailFormat implements ValidateCommunicationFormat {
    @Override
    public void validate(FormatCommunicationEnum formatCommunication, String valueMobilePhone) {
        if (FormatCommunicationEnum.EMAIL.equals(formatCommunication)
                && (!ValidationUtils.isValidEmail(valueMobilePhone))) {
            throw new FormatMailException();
        }
    }
}
