package br.com.magazineluiza.scheduler.controller.impl;

import br.com.magazineluiza.scheduler.controller.CommunicationController;
import br.com.magazineluiza.scheduler.dto.request.CreateCommunicationRequest;
import br.com.magazineluiza.scheduler.dto.response.CommunicationResponse;
import br.com.magazineluiza.scheduler.enums.StatusCommunicationEnum;
import br.com.magazineluiza.scheduler.service.CommunicationService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;

@Api(value = "Communication")
@RestController
@RequestMapping("/communication")
public class CommunicationControllerImpl implements CommunicationController {
    @Autowired
    private CommunicationService communicationService;

    @PostMapping
    public ResponseEntity<?> saveCommunication(@RequestBody @Valid CreateCommunicationRequest communicationRequest) {
        CommunicationResponse communicationResponse = communicationService.create(communicationRequest);
        URI uri = ServletUriComponentsBuilder.fromCurrentRequestUri().path("/{id}").buildAndExpand(communicationResponse.getId()).toUri();
        return ResponseEntity.created(uri).build();
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> findStatusCommunication(@PathVariable("id") Long id) {
        StatusCommunicationEnum status = communicationService.findStatusCommunicationById(id);
        return ResponseEntity.ok(status);
    }

    @PutMapping("/{id}/cancel")
    public ResponseEntity<?> cancelCommunicationById(Long id) {
        CommunicationResponse communication = communicationService.cancelCommunicationById(id);
        return ResponseEntity.ok(communication);
    }

}
