package br.com.magazineluiza.scheduler.controller;

import br.com.magazineluiza.scheduler.dto.request.CreateCommunicationRequest;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.ResponseEntity;

public interface CommunicationController {

    @ApiOperation(value = "Save a new schedule communication")
    ResponseEntity<?> saveCommunication(CreateCommunicationRequest communicationRequest);

    @ApiOperation(value = "Find status of Communication by id")
    ResponseEntity<?> findStatusCommunication(Long id);

    @ApiOperation(value = "Cancel communication by id")
    ResponseEntity<?> cancelCommunicationById(Long id);
}
