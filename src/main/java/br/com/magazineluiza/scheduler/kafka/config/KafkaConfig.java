package br.com.magazineluiza.scheduler.kafka.config;

import br.com.magazineluiza.scheduler.kafka.util.ReadFileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.listener.ContainerProperties;

import java.util.Map;

@ConditionalOnProperty("kafka.active")
@Configuration
public class KafkaConfig {
    @Autowired
    public ReadFileUtils readFileUtils;

    @Bean
    public ConsumerFactory<String, String> consumerFactory() {
        Map<String, Object> properties = readFileUtils.readKafkaConfigurations();
        return new DefaultKafkaConsumerFactory(properties);
    }

    @Bean
    public ConcurrentKafkaListenerContainerFactory<String, String> communicationKafkaListenerContainerFactory() {
        ConcurrentKafkaListenerContainerFactory<String, String> factory = new ConcurrentKafkaListenerContainerFactory<>();
        factory.setConsumerFactory(consumerFactory());
        factory.getContainerProperties().setAckMode(ContainerProperties.AckMode.RECORD);
        return factory;
    }
}
