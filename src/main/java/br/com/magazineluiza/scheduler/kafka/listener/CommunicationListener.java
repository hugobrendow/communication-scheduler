package br.com.magazineluiza.scheduler.kafka.listener;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.Headers;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import java.util.Map;

@ConditionalOnProperty("kafka.active")
@Component
public class CommunicationListener {

    @KafkaListener(topics = "communication-messages", containerFactory = "communicationKafkaListenerContainerFactory")
    public void consumeCommunicationMessages(@Payload ConsumerRecord<String, String> record,
                                             @Header(KafkaHeaders.RECEIVED_MESSAGE_KEY) Object key,
                                             @Headers Map<Object, Object> headers) {
        System.out.println("Consuming message...");
        System.out.println("Sending new message");
        System.out.println("Record: " + record.value());
    }
}
