package br.com.magazineluiza.scheduler.kafka.util;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

@Component
public class ReadFileUtils {
    @Value("${kafka.bootstrap.server}")
    private String bootstrapServer = "";
    @Value("${kafka.key.deserializer}")
    private String keyDeserializer = "";
    @Value("${kafka.value.deserializer}")
    private String valueDeserializer = "";
    @Value("${kafka.auto.offset.reset}")
    private String autoOffset = "";
    @Value("${kafka.group.id}")
    private String groupId = "";

    public final Map<String, Object> readPropertiesFile(String fileName) {
        Properties getProperties = new Properties();
        InputStream inputStream = null;
        HashMap propertyMap = new HashMap();

        try {
            inputStream = this.getClass().getClassLoader().getResourceAsStream(fileName);
            getProperties.load(inputStream);
            propertyMap.putAll((Map)getProperties);
        } catch (Exception exception) {
            exception.printStackTrace();
        }

        return (Map)propertyMap;
    }

    public final Map<String, Object> readKafkaConfigurations() {
        return new HashMap<>() {{
            put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServer);
            put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, keyDeserializer);
            put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
            put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
            put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, valueDeserializer);
            put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, autoOffset);
            put(ConsumerConfig.GROUP_ID_CONFIG, groupId);
        }};
    }
}
