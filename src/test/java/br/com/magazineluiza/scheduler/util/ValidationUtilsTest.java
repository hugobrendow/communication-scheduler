package br.com.magazineluiza.scheduler.util;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class ValidationUtilsTest {

    @Test
    void testValidMobileNumber() {
        assertThat(ValidationUtils.isValidMobileNumber("(61) 98832-4455")).isTrue();
    }

    @Test
    void testInvalidValidMobileNumber() {
        assertThat(ValidationUtils.isValidMobileNumber("(61) 985-9966")).isFalse();
    }

    @Test
    void testValidEmailAddress() {
        assertThat(ValidationUtils.isValidEmail("teste@gmail.com")).isTrue();
    }

    @Test
    void testInvalidEmailAddress() {
        assertThat(ValidationUtils.isValidEmail("teste@gcom")).isFalse();
    }

}
