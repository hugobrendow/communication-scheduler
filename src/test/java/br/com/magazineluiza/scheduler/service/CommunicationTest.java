package br.com.magazineluiza.scheduler.service;

import br.com.magazineluiza.scheduler.dto.request.CreateCommunicationRequest;
import br.com.magazineluiza.scheduler.dto.response.CommunicationResponse;
import br.com.magazineluiza.scheduler.enums.FormatCommunicationEnum;
import br.com.magazineluiza.scheduler.enums.StatusCommunicationEnum;
import br.com.magazineluiza.scheduler.exception.FormatPhoneNumberException;
import br.com.magazineluiza.scheduler.exception.NotFoundCommunicationException;
import br.com.magazineluiza.scheduler.model.Communication;
import br.com.magazineluiza.scheduler.repository.CommunicationRepository;
import org.junit.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.time.LocalDateTime;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertThrows;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class CommunicationTest {

    @Autowired
    CommunicationService communicationService;

    @MockBean
    private CommunicationRepository communicationRepository;

    private final static Long ID = 1L;

    CreateCommunicationRequest createCommunicationRequest = new CreateCommunicationRequest(LocalDateTime.now(),
            "hugoteste@gmail.com", "Welcome to Magalu", FormatCommunicationEnum.EMAIL);
    Communication communication = new Communication(ID, LocalDateTime.now(), "hugoteste@gmail.com", "Welcome to Magalu", StatusCommunicationEnum.SCHEDULED,
            FormatCommunicationEnum.EMAIL);


    @BeforeEach
    public void setup() {
        Mockito.when(communicationRepository.save(Mockito.any())).thenReturn(communication);
        Mockito.when(communicationRepository.findById(ID)).thenReturn(Optional.of(communication));
    }

    @Test
    public void createCommunicationTest() {
        CommunicationResponse communicationResponse = communicationService.create(createCommunicationRequest);
        assertThat(communicationResponse.getStatusCommunication()).isEqualTo(StatusCommunicationEnum.SCHEDULED);
        assertThat(communicationResponse.getId()).isEqualTo(ID);
    }

    @Test
    public void createCommunicationInvalidFormatTest() {
        createCommunicationRequest.setFormatCommunication(FormatCommunicationEnum.SMS);
        assertThrows(FormatPhoneNumberException.class, () ->
                communicationService.create(createCommunicationRequest));

    }

    @Test
    public void findStatusCommunicationByIdTest() {
        StatusCommunicationEnum statusCommunicationEnum = communicationService.findStatusCommunicationById(ID);
        assertThat(statusCommunicationEnum).isEqualTo(StatusCommunicationEnum.SCHEDULED);
    }

    @Test
    public void findStatusCommunicationByIdNotExistTest() {
        assertThrows(NotFoundCommunicationException.class, () ->
                communicationService.findStatusCommunicationById(100L));
    }

    @Test
    public void cancelCommunicationByIdTest() {
        CommunicationResponse communicationResponse = communicationService.cancelCommunicationById(ID);
        assertThat(communicationResponse.getStatusCommunication()).isEqualTo(StatusCommunicationEnum.CANCELLED);
    }

    @Test
    public void cancelCommunicationByIdNotExistTest() {
        assertThrows(NotFoundCommunicationException.class, () ->
                communicationService.cancelCommunicationById(100L));
    }
}
